with Ada.Characters.Latin_1; use Ada.Characters.Latin_1;
with Ada.Strings.Fixed; use Ada.Strings.Fixed;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Text_IO; use Ada.Text_IO;

package body Ansi is
    function Escape(Self : Color; Bright : Boolean := False) return Unbounded_String is
        Baseval : Natural := (case Self is
                when Black => 0,
                when Red => 1,
                when Green => 2,
                when Yellow => 3,
                when Blue => 4,
                when Magenta => 5,
                when Cyan => 6,
                when White => 7);
        Colorval : Natural := (if Bright then
            Baseval + 90
        else
            Baseval + 30);
        Arg : String := Trim (Natural'Image (Colorval), Ada.Strings.Left);
    begin
        return To_Unbounded_String (ESC & '[' & Arg & 'm');
    end;
    
    function Escape(Self : Style) return Unbounded_String is
        Val : Natural := (case Self is
                when Reset => 0,
                when Bold => 1,
                when Italic => 3,
                when Underline => 4,
                when Strikethrough => 9);
        Arg : String := Trim (Natural'Image (Val), Ada.Strings.Left);
    begin
        return To_Unbounded_string(ESC & '[' & Arg & 'm');
    end;
    
    procedure Put(Self : Style) is
    begin
        Put (To_String (Escape (Self)));
    end;
end Ansi;

