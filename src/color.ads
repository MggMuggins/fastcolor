with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

with Ansi;

package Color is
    type Color is (Red, Orange, Yellow, Green, Blue, Purple);
    
    Clear : Unbounded_String := Ansi.Escape (Ansi.Reset);
    
    function Escape_Color (Hue : Color) return Unbounded_String;
    
    procedure Put (Self : Color);
    
end Color;

