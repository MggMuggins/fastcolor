with Ada.Characters.Handling; use Ada.Characters.Handling;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

with Ansi; use Ansi;
with Color; use Color;

package body Escape is
    function Style_From (Self : Character) return Ansi.Style is
        Modifier : Style := (case Self is
                when '*' => Bold,
                when '_' => Underline,
                when '/' => Italic,
                when '-' => Strikethrough,
                when others => raise BadFormatChar with "Invalid Style Char");
    begin
        return Modifier;
    end;
    
    function Color_From (Self : Character) return Color.Color is
        Hue : Color.Color := (case To_Lower (Self) is
                when 'r' => Red,
                when 'o' => Orange,
                when 'y' => Yellow,
                when 'g' => Green,
                when 'b' => Blue,
                when 'p' => Purple,
                when others => raise BadFormatChar with "Invalid Color Char");
    begin
        return Hue;
    end;
end Escape;

