with Ada.Characters.Latin_1; use Ada.Characters.Latin_1;
with Ada.Strings.Maps; use Ada.Strings.Maps;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Text_IO; use Ada.Text_IO;

with Ansi;
with Color;
with Escape;

procedure Fastcolor is
    Char : Character;
    
    type State is (Normal,
        Need_Color, Need_Format, Need_Extension,
        Format_Word, Format_Line);
    
    Current_State : State := Normal;
begin
    loop
        exit when End_Of_File;
        Get (Char);
        
        case Current_State is
            when Normal =>
                if Char = '\' then
                    Current_State := Need_Color;
                else
                    Put (Char);
                end if;
            when Need_Color =>
                Color.Put (Escape.Color_From (Char));
                Current_State := Need_Format;
            when Need_Format =>
                begin
                    Ansi.Put (Escape.Style_From (Char));
                exception
                    when Escape.BadFormatChar => null;
                    when others => raise;
                end;
                Current_State := Need_Extension;
            when Need_Extension =>
                if Char = '>' then
                    Current_State := Format_Line;
                else
                    Put (Char);
                    Current_State := Format_Word;
                end if;
            when Format_Line =>
                if End_Of_Line then
                    Put (To_String (Color.Clear));
                end if;
                
                if Char = '\' then
                    Current_State := Need_Color;
                else
                    Put (Char);
                    Current_State := Normal;
                end if;
            when Format_Word =>
                if Is_In (Char, Escape.Word_Separators) then
                    Put (To_String (Color.Clear));
                end if;
                
                if Char = '\' then
                    Current_State := Need_Color;
                else
                    Put (Char);
                    Current_State := Normal;
                end if;
        end case;
        
        if End_Of_Line then
            Put (To_String (Color.Clear));
            New_Line;
        end if;
    end loop;
end;

