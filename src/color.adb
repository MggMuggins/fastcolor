with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Text_IO; use Ada.Text_IO;

with Ansi; use Ansi;
with Color; use Color;

package body Color is
    function Escape_Color (Hue : Color) return Unbounded_String is
        Color_Escape : Unbounded_String := (case Hue is
                when Red => Ansi.Escape (Ansi.Red),
                when Orange => Ansi.Escape (Ansi.Red, True),
                when Yellow => Ansi.Escape (Ansi.Yellow),
                when Green => Ansi.Escape (Ansi.Green),
                when Blue => Ansi.Escape (Ansi.Blue),
                when Purple => Ansi.Escape (Ansi.Magenta)
            );
    begin
        return Color_Escape;
    end;
    
    procedure Put (Self : Color) is
    begin
        Put (To_String (Escape_Color (Self)));
    end;
end Color;

