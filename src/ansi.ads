with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package Ansi is
    type Color is (Black, Red, Green, Yellow, Blue, Magenta, Cyan, White);
    type Style is (Reset, Bold, Italic, Underline, Strikethrough);
    
    function Escape (Self : Color; Bright : Boolean := False) return Unbounded_String;
    function Escape (Self : Style) return Unbounded_String;
    
    procedure Put (Self : Style);
end Ansi;

