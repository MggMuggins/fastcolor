with Ada.Characters.Latin_1; use Ada.Characters.Latin_1;
with Ada.Strings.Maps; use Ada.Strings.Maps;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

with Ansi; use Ansi;
with Color; use Color;

package Escape is
    BadFormatChar : exception;
    
    Word_Separators : Character_Set := To_Set (" ()[]{}<>.,|/\" & LF);
    
    function Style_From (Self : Character) return Ansi.Style;
    
    function Color_From (Self : Character) return Color.Color;
    
end Escape;

